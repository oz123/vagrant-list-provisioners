# Copyright 2023 Oz Tiram <oz.tiram@gmail.com>
#
#			MIT License
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

class ListProvisionersCommand < Vagrant.plugin(2, :command)
  def self.synopsis
    "list all named provisioners in a Vagrantfile"
  end

  def execute
    # Initialize an empty dictionary to store the provisioners
    provisioners = {}

    # Iterate over each VM
    with_target_vms(nil, single_target: false) do |vm|
    # Get the provisioners for the VM
    vm_provisioners = vm.config.vm.provisioners.map do |provisioner|
      # Skip provisioners of type 'file'
      next if provisioner.type == :file
        provisioner.name
      end
      # Add the provisioners to the dictionary
      provisioners[vm.name] = vm_provisioners.compact
    end
    # Iterate over each VM
    # Print the provisioners in a table format
    provisioners.each do |vm_name, vm_provisioners|
      @env.ui.info("#{vm_name}:")
      vm_provisioners.each do |provisioner|
        @env.ui.info("  - #{provisioner}")
      end
    end

    0
  end
end

class ListProvisionersPlugin < Vagrant.plugin(2)
  name "List Provisioners Plugin"

  command "list-provisioners" do
    ListProvisionersCommand
  end
end
