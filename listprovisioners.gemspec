Gem::Specification.new do |s|
  s.name        = 'listprovisioners'
  s.version     = '0.1.0'
  s.author      = 'Oz Tiram'
  s.email       = 'oz.tiram@gmail.com'
  s.summary     = 'List provisioners in Vagrantfile'
  s.description = 'A Vagrant plugin that adds a command to list provisioners in the Vagrantfile'
  s.homepage    = 'https://github.com/oz123/listprovisioners'
  s.license     = 'MIT'
  s.required_ruby_version = '>= 2.0.0'
  s.files = Dir["lib/**/*"]
end
