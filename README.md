Vagrant List Provisioners
=========================

The purpose of this plugin is to make own life easier by not having to remember which
named provisioners are defined in the `Vagrantfile`.

Usage
-----

```
$ vagrant list-provisioners
```

The plugin will then list all provisioners defined for each machine in the file.

```
$ vagrant list-provisioners
vm1:
  - common
  - foo
  - bar
  - baz
vm2:
  - common
  - foo
  - bar
  - baz
```

See example file added in this repository.


Installing
----------

Clone this repository:

```
$ git clone git@gitlab.com:oz123/vagrant-list-provisioners.git
```

Then create a gem with:
```
$ cd vagrant-list-provisioners
$ gem build listprovisioners.gemspec
```

Install the plugin with:
```
vagrant plugin install ./listprovisioners-0.1.0.gem
```
